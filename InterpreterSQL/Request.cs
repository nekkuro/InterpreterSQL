﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpreterSQL
{
    class Request
    {
        private List<string> select_column;
        private string table;
        private string expression;
        public Request()
        {
            select_column = new List<string>();
            table = "";
            expression = "";
        }
        public List<string> GetSelectColumn()
        {
            return select_column;
        }
        public void SetSelectColumn(List<string> _select_column)
        {
            select_column = _select_column;
        }
        public string GetTable()
        {
            return table;
        }
        public void SetTable(string _table)
        {
            table = _table;
        }
        public string GetExpression()
        {
            return expression;
        }
        public void SetExpression(string _expression)
        {
            expression = _expression;
        }
    }
}
