﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Xml;

namespace InterpreterSQL
{
    class DataController
    {
        private DataSet dataSet;
        private List<string[]> result;
        List<string> select_columns;
        public DataController()
        {
            dataSet = CreateDataSet();
            result = new List<string[]>();
            select_columns = new List<string>();
        }
        public List<string[]> GetDataFromRequest(Request request)
        {
            DataTable table = GetTableByName(request.GetTable());
            if(table != null)
            {
                DataRow[] select_result = table.Select(request.GetExpression());
                if (select_result.Length != 0)
                {
                    if (request.GetSelectColumn()[0] == "*")
                    {
                        foreach (DataColumn dc in table.Columns)
                        {
                            select_columns.Add(dc.ColumnName);
                        }
                    }
                    else
                    {
                        select_columns = request.GetSelectColumn();
                    }
                    foreach (DataRow row in select_result)
                    {
                        string[] data = new string[select_columns.Count];
                        int position = 0;
                        foreach (string column in select_columns)
                        {
                            data[position] = row[column].ToString();
                            position++;
                        }
                        result.Add(data);
                    }
                }
            }
            return result;
        }
        public List<string> GetColumnName()
        {
            return select_columns;
        }
        private DataTable GetTableByName(string table_name)
        {
            foreach(DataTable table in dataSet.Tables)
            {
                if(table.TableName.Equals(table_name))
                {
                    return table;
                }
            }
            return null;
        }
        public DataSet CreateDataSet()
        {
            XmlReader xmlFile;
            xmlFile = XmlReader.Create("..\\..\\Database.xml", new XmlReaderSettings());
            DataSet ds = new DataSet();
            ds.ReadXml(xmlFile);
            return ds;
        }
    }
}
