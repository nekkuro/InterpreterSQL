﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpreterSQL
{
    class InterpreterNode
    {
        private LexicalItem data;

        public InterpreterNode()
        {
            data = null;
        }
        public InterpreterNode(Constant.LexemType type, string lexem)
        {
            data = new LexicalItem(type, lexem);
        }
        public Constant.LexemType GetLexemType()
        {
            return data.GetLexemType();
        }
        public string GetLexem()
        {
            return data.GetLexem();
        }
        public void SetData(Constant.LexemType type, string lexem)
        {
            data = new LexicalItem(type, lexem);
        }
    }
}
