﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpreterSQL
{
    class SintaxAnalyzer
    {
        private InterpreterTree result;
        private List<LexicalItem> result_from_lexer;
        private int string_number;
        private int error_count;
        private List<String> error;



        public SintaxAnalyzer(List<LexicalItem> _result_from_lexer)
        {
            error_count = 0;
            result_from_lexer = _result_from_lexer;
            error = new List<string>();
            string_number = -1;
            result = new InterpreterTree(new InterpreterNode(Constant.LexemType.SYSTEM, "PROG"));
        }

        private void Error()
        {
            string error_text = "Синтаксическая ошибка в строке: " + (string_number).ToString() + " - " + GetThisLexemType() + " " + GetThisLexem();
            if (!error.Contains(error_text))
            {
                error_count++;
                error.Add(error_text);
            }
        }
        private void EOFError()
        {
            string error_text = "Синтаксическая ошибка: нет символа сигнализирующего об окончании запроса - \";\"";
            if (!error.Contains(error_text))
            {
                error_count++;
                error.Add(error_text);
            }
        }
        private void NextString()
        {
            if (string_number < result_from_lexer.Count - 1)
            {
                string_number++;
            }
        }
        private void PrevString()
        {
            if (string_number > 1)
            {
                string_number--;
            }
        }
        private Constant.LexemType GetThisLexemType()
        {
            return result_from_lexer[string_number].GetLexemType();
        }
        private string GetThisLexem()
        {
            return result_from_lexer[string_number].GetLexem();
        }
        public int GetErrorCount()
        {
            return error_count;
        }
        public List<string> GetError()
        {
            return error;
        }
        public InterpreterTree GetResultTree()
        {
            return result;
        }

        private bool CheckNumber(string _value)
        {
            int number;
            bool result = Int32.TryParse(_value, out number);
            return result;
        }

        public bool Start()
        {
            for (string_number = 0; string_number < result_from_lexer.Count; string_number++)
            {
                InterpreterTree sintax_tree = new InterpreterTree(new InterpreterNode(Constant.LexemType.SYSTEM, "START"));
                if (result_from_lexer[result_from_lexer.Count - 1].GetLexemType() != Constant.LexemType.EOF)
                {
                    EOFError();
                    return false;
                }
                if (!SelectStatement(sintax_tree))
                {
                    Error();
                    return false;
                }
                else
                {
                    result.Add(sintax_tree);
                    sintax_tree = new InterpreterTree(new InterpreterNode(Constant.LexemType.SYSTEM, "START"));
                }
                if(GetThisLexemType()!=Constant.LexemType.EOF)
                {
                    Error();
                    return false;
                }
                if (error_count != 0)
                {
                    return false;
                }
            }
            return true;
        }
        private bool SelectStatement(InterpreterTree sintax_tree)
        {
            InterpreterTree temp_tree = new InterpreterTree();
            if (GetThisLexemType()==Constant.LexemType.KEYWORD
                && GetThisLexem() == "SELECT")
            {
                temp_tree = new InterpreterTree(new InterpreterNode(Constant.LexemType.SELECT, GetThisLexem()));
                sintax_tree.Add(temp_tree);
                NextString(); 
                if (!ColumnList(sintax_tree.GetLocalChildren()[sintax_tree.GetLocalChildren().Count - 1]))
                {
                    Error();
                    return false;
                }
                if (!FromStatement(sintax_tree))
                {
                    Error();
                    return false;
                }
            }
            else
            {
                Error();
                return false;
            }
            return true;
        }
        private bool FromStatement(InterpreterTree sintax_tree)
        {
            InterpreterTree temp_tree;
            if(GetThisLexemType() == Constant.LexemType.KEYWORD && GetThisLexem()=="FROM")
            {
                temp_tree = new InterpreterTree(new InterpreterNode(Constant.LexemType.FROM, GetThisLexem()));
                sintax_tree.Add(temp_tree);
                NextString();
                if (!Table(sintax_tree.GetLocalChildren()[sintax_tree.GetLocalChildren().Count - 1]))
                {
                    Error();
                    return false;
                }
                NextString();
                if(GetThisLexemType()==Constant.LexemType.EOF)
                {
                    return true;
                }
                else
                {
                    if (!WhereStatement(sintax_tree))
                    {
                        Error();
                        return false;
                    }
                }
            }
            else
            {
                Error();
                return false;
            }
            return true;
        }
        private bool WhereStatement(InterpreterTree sintax_tree)
        {
            InterpreterTree temp_tree;
            if (GetThisLexemType() == Constant.LexemType.KEYWORD && GetThisLexem() == "WHERE")
            {
                temp_tree = new InterpreterTree(new InterpreterNode(Constant.LexemType.WHERE, GetThisLexem()));
                sintax_tree.Add(temp_tree);
                NextString();
                if (!Expressions(sintax_tree.GetLocalChildren()[sintax_tree.GetLocalChildren().Count - 1]))
                {
                    Error();
                    return false;
                }
            }
            else
            {
                Error();
                return false;
            }
            return true;
        }
        private bool ColumnList(InterpreterTree sintax_tree)
        {
            if (Column(sintax_tree) || GetThisLexemType() == Constant.LexemType.ASTERIKS)
            {
                NextString();
                bool columns = true;
                while (columns)
                {
                    if (GetThisLexemType() == Constant.LexemType.COMMA)
                    {
                        NextString();
                        if (!Column(sintax_tree))
                        {
                            Error();
                            return false;
                        }
                    }
                    else
                    {
                        columns = false;
                    }
                }
            }
            return true;
        }
        private bool Column(InterpreterTree sintax_tree)
        {
            InterpreterTree temp_tree;
            if (GetThisLexemType() == Constant.LexemType.IDENTIFICATOR)
            {
                temp_tree = new InterpreterTree(new InterpreterNode(Constant.LexemType.COLUMN, GetThisLexem()));
                sintax_tree.Add(temp_tree);
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool Table(InterpreterTree sintax_tree)
        {
            InterpreterTree temp_tree;
            if (GetThisLexemType() == Constant.LexemType.IDENTIFICATOR)
            {
                temp_tree = new InterpreterTree(new InterpreterNode(Constant.LexemType.TABLE, GetThisLexem()));
                sintax_tree.Add(temp_tree);
                return true;
            }
            else
            {
                Error();
                return false;
            }
        }
        private bool Expressions(InterpreterTree sintax_tree)
        {
            InterpreterTree temp_tree;
            temp_tree = new InterpreterTree(new InterpreterNode(Constant.LexemType.EXPR, "EXPR"));
            sintax_tree.Add(temp_tree);
            if (!predicate(sintax_tree.GetLocalChildren()[sintax_tree.GetLocalChildren().Count - 1]))
            {
                Error();
                return false;
            }
            if (GetThisLexemType() != Constant.LexemType.EOF)
            {
                if(GetThisLexemType()==Constant.LexemType.KEYWORD &&
                    (GetThisLexem()=="AND" || GetThisLexem()=="OR"))
                {
                    if (GetThisLexem() == "AND")
                    {
                        temp_tree = new InterpreterTree(new InterpreterNode(Constant.LexemType.AND, GetThisLexem()));
                    }
                    else
                    {
                        temp_tree = new InterpreterTree(new InterpreterNode(Constant.LexemType.OR, GetThisLexem()));
                    }
                    sintax_tree.Add(temp_tree);
                    NextString();
                    if (!Expressions(sintax_tree))
                    {
                        Error();
                        return false;
                    }
                }
                else
                {
                    Error();
                    return false;
                }
            }
            return true;
        }
        private bool predicate(InterpreterTree sintax_tree)
        {
            if (AtomareExpressions(sintax_tree))
            {
                NextString();
                if (Operator(sintax_tree))
                {
                    NextString();
                    if (AtomareExpressions(sintax_tree))
                    {
                        NextString();
                        return true;
                    }
                }
            }
            return true;
        }
        private bool AtomareExpressions(InterpreterTree sintax_tree)
        {
            if (Column(sintax_tree) || Literal(sintax_tree))
            {
                return true;
            }
            else
            {
                Error();
                return false;
            }
        }
        private bool Literal(InterpreterTree sintax_tree)
        {
            InterpreterTree temp_tree;
            if (GetThisLexemType() == Constant.LexemType.LITERAL)
            {
                if (GetThisLexem()[0] == '\'')
                {
                    temp_tree = new InterpreterTree(new InterpreterNode(Constant.LexemType.TEXT, GetThisLexem()));
                }
                else
                {
                    if (CheckNumber(GetThisLexem()))
                    {
                        temp_tree = new InterpreterTree(new InterpreterNode(Constant.LexemType.NUMBER, GetThisLexem()));
                    }
                    else
                    {
                        Error();
                        return false;
                    }
                }
                sintax_tree.Add(temp_tree);
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool Operator(InterpreterTree sintax_tree)
        {
            InterpreterTree temp_tree;
            if (GetThisLexemType() == Constant.LexemType.OPERATOR)
            {
                temp_tree = new InterpreterTree(new InterpreterNode(GetThisLexemType(), GetThisLexem()));
                sintax_tree.Add(temp_tree);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
