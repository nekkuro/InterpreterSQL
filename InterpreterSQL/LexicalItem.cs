﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpreterSQL
{
    class LexicalItem
    {
        private Constant.LexemType type;
        private string lexem;
        public LexicalItem(Constant.LexemType _type, string _lexem)
        {
            type = _type;
            lexem = _lexem;
        }
        public Constant.LexemType GetLexemType()
        {
            return type;
        }
        public string GetLexem()
        {
            return lexem;
        }
    }
}
