﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace InterpreterSQL
{
    class Program
    {
        static void Main(string[] args)
        {
            string request = "";
            List<string> errors = new List<string>();
            bool error = false;
            try
            {
                if(!error)
                {
                    request = ReadRequestFromFile();
                }
            }
            catch(Exception ex)
            {
                error = true;
                Console.WriteLine("Error read file");
            }
            

            DataController dataController = new DataController();
            try
            {
                if(!error)
                {
                    dataController.CreateDataSet();
                }
            }
            catch(Exception ex)
            {
                error = true;
                Console.WriteLine("Error create DataSet");
            }
            
            LexicalAnalyzer lexer = new LexicalAnalyzer();
            try
            {
                if(!error)
                {
                    lexer.Scanner(request);
                }
            }
            catch (Exception ex)
            {
                error = true;
                Console.WriteLine("Lexical rrors:");
                errors = lexer.GetError();
                foreach (string er in errors)
                {
                    Console.WriteLine(er);
                }
            }
            SintaxAnalyzer sintax = new SintaxAnalyzer(lexer.GetLexicalItem());
            try
            {
                if(!error)
                {
                    bool sint = sintax.Start();
                    if(!sint)
                    {
                        error = true;
                        Console.WriteLine("Sintax errors:");
                        errors = sintax.GetError();
                        foreach (string er in errors)
                        {
                            Console.WriteLine(er);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error = true;
                Console.WriteLine("Sintax errors:");
                errors = sintax.GetError();
                foreach (string er in errors)
                {
                    Console.WriteLine(er);
                }
            }
            Translator translator = new Translator();
            try
            {
                if(!error)
                {
                    translator.Translate(sintax.GetResultTree());
                }
            }
            catch (Exception ex)
            {
                error = true;
                Console.WriteLine("Translator errors:");
                errors = sintax.GetError();
                foreach (string er in errors)
                {
                    Console.WriteLine(er);
                }
            }
            List<string[]> result = dataController.GetDataFromRequest(translator.GetRequest());
            PrintResult(result, dataController.GetColumnName());
            Console.Write("Press any key to exit...");
            Console.ReadKey();
        }
        static void Print(InterpreterTree tree, int k)
        {
            for (int i = 0; i < k; i++)
            {
                Console.Write("   ");
            }
            Console.Write(tree.GetData().GetLexemType());
            Console.Write(" ");
            Console.WriteLine(tree.GetData().GetLexem());
            k++;
            foreach(InterpreterTree child in tree.GetLocalChildren())
            {
                Print(child, k);
            }
        }
        static string ReadRequestFromFile()
        {
            string result = "";
            string line;

            System.IO.StreamReader file = new System.IO.StreamReader(@"..\\..\\request.txt");
            while ((line = file.ReadLine()) != null)
            {
                result += line;
                result += "\n";
            }

            file.Close();
            return result;
        }
        static void PrintResult(List<string[]> results, List<string> columns)
        {
            foreach(string column in columns)
            {
                Console.Write("{0, 20} | ", column);
            }
            Console.WriteLine();
            foreach (string[] result in results)
            {
                foreach(string res in result)
                {
                    Console.Write("{0, 20} | ", res);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
