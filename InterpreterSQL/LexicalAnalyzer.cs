﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpreterSQL
{
    class LexicalAnalyzer
    {
        private List<string> keywords;
        private int string_number, pos;
        private string work_string;
        private string output_text;
        private int error_count;
        private List<String> error;
        private List<LexicalItem> lexicalItem;

        public LexicalAnalyzer()
        {
            keywords = new List<string>() { "select", "from", "where", "or", "and" };
            output_text = "";
            string_number = 0;
            pos = 0;
            work_string = "";
            error_count = 0;
            error = new List<string>();
            lexicalItem = new List<LexicalItem>(); 
        }
        public string GetResult()
        {
            return output_text;
        }
        public int GetErrorCount()
        {
            return error_count;
        }
        public List<string> GetError()
        {
            return error;
        }
        public List<LexicalItem> GetLexicalItem()
        {
            return lexicalItem;
        }
        private void WriteError(int position, int number)
        {
            error_count++;
            error.Add("Произошла ошибка в строке " + number.ToString() + ", символ " + position.ToString());
        }

        private void AddOutputCode(string str)
        {
            output_text += str;
        }
        private bool CheckSkip(char check)
        {
            if (check == ' ' || check == '\t' || check == '\n' || check == '\f')
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool CheckLetter(char _letter)
        {
            if ((_letter >= 'A' && _letter <= 'Z') || (_letter >= 'a' && _letter <= 'z'))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool CheckDecimal(char _decimal)
        {
            if ((_decimal >= '0' && _decimal <= '9'))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool CheckQuote(char _quote)
        {
            if (_quote == '\''
                || _quote == '\"')
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void Keywords(string lexeme)
        {
            foreach(string kw in keywords)
            {
                if (kw.Equals(lexeme))
                {
                    AddOutputCode(lexeme.ToUpper());
                    lexicalItem.Add(new LexicalItem(Constant.LexemType.KEYWORD, lexeme.ToUpper()));
                    return;
                }
            }
            Column(lexeme);
            return;
        }
        private void Column(string lexeme)
        {
            if (CheckLetter(work_string[pos]))
            {
                lexeme += work_string[pos];
                pos++;
                if (pos < work_string.Length)
                {
                    while (CheckLetter(work_string[pos]))
                    {
                        lexeme += work_string[pos];
                        pos++;
                    }
                    pos--;
                }
            }
            lexicalItem.Add(new LexicalItem(Constant.LexemType.IDENTIFICATOR, lexeme));
            AddOutputCode("IDENTIFICATOR(" + lexeme + ")");
            return;
        }
        private void Literal(string lexeme)
        {
            if (CheckDecimal(work_string[pos])
                || CheckQuote(work_string[pos]))
            {
                lexeme += work_string[pos];
                pos++;
                if (pos < work_string.Length)
                {
                    while (CheckDecimal(work_string[pos]) 
                        || CheckLetter(work_string[pos])
                        || CheckQuote(work_string[pos]))
                    {
                        lexeme += work_string[pos];
                        pos++;
                    }
                    pos--;
                }
            }
            lexicalItem.Add(new LexicalItem(Constant.LexemType.LITERAL, lexeme));
            AddOutputCode("LITERAL(" + lexeme + ")");
            return;
        }

        public string Scanner(string _input_test)
        {
            _input_test = _input_test.ToLower();
            string[] split_text = _input_test.Split('\n');
            string lexeme;
            for (string_number = 0; string_number < split_text.Length; string_number++)
            {
                work_string = split_text[string_number];
                for (pos = 0; pos < work_string.Length; pos++)
                {
                    lexeme = "";
                    if (CheckSkip(work_string[pos]))
                    {
                        AddOutputCode(" ");
                    }
                    else
                    {
                        if (CheckLetter(work_string[pos]))
                        {
                            lexeme += work_string[pos];
                            pos++;
                            while (pos < work_string.Length && (CheckLetter(work_string[pos]) || work_string[pos] == '_' || CheckDecimal(work_string[pos])))
                            {
                                lexeme += work_string[pos];
                                pos++;
                            }
                            Keywords(lexeme);
                            pos--;
                        }
                        else
                        {
                            if (CheckDecimal(work_string[pos])
                                || CheckQuote(work_string[pos]))
                            {
                                Literal(lexeme);
                            }
                            else
                            {
                                switch (work_string[pos])
                                {
                                    case '*':
                                        AddOutputCode("ASTERIKS");
                                        lexicalItem.Add(new LexicalItem(Constant.LexemType.ASTERIKS, "ASTERIKS"));
                                        break;
                                    case '=':
                                        AddOutputCode("EQ");
                                        lexicalItem.Add(new LexicalItem(Constant.LexemType.OPERATOR, "EQ"));
                                        break;
                                    case '<':
                                        if (pos < work_string.Length)
                                        {
                                            pos++;
                                            if (work_string[pos] == '=')
                                            {
                                                AddOutputCode("GE");
                                                lexicalItem.Add(new LexicalItem(Constant.LexemType.OPERATOR, "GE"));
                                            }
                                            else
                                            {
                                                AddOutputCode("GT");
                                                lexicalItem.Add(new LexicalItem(Constant.LexemType.OPERATOR, "GT"));
                                                pos--;
                                            }
                                        }
                                        break;
                                    case '>':
                                        if (pos < work_string.Length)
                                        {
                                            pos++;
                                            if (work_string[pos] == '=')
                                            {
                                                AddOutputCode("NE");
                                                lexicalItem.Add(new LexicalItem(Constant.LexemType.OPERATOR, "NE"));
                                            }
                                            else
                                            {
                                                AddOutputCode("LE");
                                                lexicalItem.Add(new LexicalItem(Constant.LexemType.OPERATOR, "LE"));
                                                pos--;
                                            }
                                        }
                                        break;
                                    case ',':
                                        AddOutputCode("COMMA");
                                        lexicalItem.Add(new LexicalItem(Constant.LexemType.COMMA, work_string[pos].ToString()));
                                        break;
                                    case ';':
                                        AddOutputCode("EOF");
                                        lexicalItem.Add(new LexicalItem(Constant.LexemType.EOF, work_string[pos].ToString()));
                                        break;
                                    default:
                                        WriteError(pos, string_number);
                                        break;
                                }
                            }
                        }
                    }
                }
                AddOutputCode("\n");
            }
            return output_text;
        }
        
    }
}
