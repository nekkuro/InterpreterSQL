﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpreterSQL
{
    class Translator
    {
        private List<string> column;
        private Request request;
        private int error_count;
        private List<String> error;

        public Translator()
        {
            column = new List<string>();
            request = new Request();
            error = new List<string>();
            error_count = 0;
        }
        private void Error()
        {
            string error_text = "Ошибка при странсляции, в условии where есть столбцы которых нет в select";
            if (!error.Contains(error_text))
            {
                error_count++;
                error.Add(error_text);
            }
        }
        public int GetErrorCount()
        {
            return error_count;
        }
        public List<string> GetError()
        {
            return error;
        }
        private bool CheckColumnInSelect(string find_column)
        {
            if (column.Contains("*"))
            {
                return true;
            }
            foreach(string clmn in column)
            {
                if(clmn.Equals(find_column))
                {
                    return true;
                }
            }
            return false;
        }
        public bool Translate(InterpreterTree input_tree)
        {
            if (input_tree.GetLocalChildren() != null)
            {
                foreach(InterpreterTree start_tree in input_tree.GetLocalChildren())
                {
                    foreach (InterpreterTree tree in start_tree.GetLocalChildren())
                    {
                        switch(tree.GetData().GetLexemType())
                        {
                            case Constant.LexemType.SELECT:
                                Select(tree);
                                break;
                            case Constant.LexemType.FROM:
                                From(tree);
                                break;
                            case Constant.LexemType.WHERE:
                                Where(tree);
                                break;
                        }
                    }
                }
            }
            else
            {
                return false;
            }
            if (error_count != 0)
            {
                return false;
            }
            return true;
        }
        private void Select(InterpreterTree input_tree)
        {
            column = new List<string>();
            if (input_tree.GetLocalChildren().Count == 0)
            {
                column.Add("*");
            }
            else
            {
                foreach(InterpreterTree tree in input_tree.GetLocalChildren())
                {
                    if (tree.GetData().GetLexemType() == Constant.LexemType.COLUMN)
                    {
                        column.Add(tree.GetData().GetLexem());
                    }
                }
            }
            request.SetSelectColumn(column);
        }
        private void From(InterpreterTree input_tree)
        {
            string table = "";
            if (input_tree.GetLocalChildren()[0].GetData().GetLexemType() == Constant.LexemType.TABLE)
            {
                table = input_tree.GetLocalChildren()[0].GetData().GetLexem();
            }
            request.SetTable(table);
        }
        private void Where(InterpreterTree input_tree)
        {
            string expr = "";
            foreach(InterpreterTree tree in input_tree.GetLocalChildren())
            {
                if (tree.GetData().GetLexemType() == Constant.LexemType.EXPR)
                {
                    foreach (InterpreterTree expr_tree in tree.GetLocalChildren())
                    {
                        switch (expr_tree.GetData().GetLexemType())
                        {
                            case Constant.LexemType.NUMBER:
                                expr += expr_tree.GetData().GetLexem() + " ";
                                break;
                            case Constant.LexemType.TEXT:
                                expr += expr_tree.GetData().GetLexem() + " ";
                                break;
                            case Constant.LexemType.OPERATOR:
                                switch(expr_tree.GetData().GetLexem())
                                {
                                    case "EQ":
                                        expr += "= ";
                                        break;
                                    case "GE":
                                        expr += "<= ";
                                        break;
                                    case "GT":
                                        expr += "< ";
                                        break;
                                    case "NE":
                                        expr += ">= ";
                                        break;
                                    case "LE":
                                        expr += "> ";
                                        break;
                                }
                                break;
                            case Constant.LexemType.COLUMN:
                                if (CheckColumnInSelect(expr_tree.GetData().GetLexem()))
                                {
                                    expr += expr_tree.GetData().GetLexem() + " ";
                                }
                                else
                                {
                                    Error();
                                }
                                break;
                        }
                    }
                }
                else
                {
                    if(tree.GetData().GetLexemType() == Constant.LexemType.AND)
                    {
                        expr += "AND ";
                    }
                    else
                    {
                        if (tree.GetData().GetLexemType() == Constant.LexemType.OR)
                        {
                            expr += "OR ";
                        }
                    }
                }
            }
            request.SetExpression(expr);
        }
        public Request GetRequest()
        {
            return request;
        }
    }
}
