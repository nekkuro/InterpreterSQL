﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpreterSQL
{
    class InterpreterTree
    {
        private InterpreterNode data;
        private List<InterpreterTree> localChildren;
        public InterpreterTree()
        {
            data = new InterpreterNode();
            localChildren = new List<InterpreterTree>();
        }
        public InterpreterTree(InterpreterNode node)
        {
            localChildren = new List<InterpreterTree>();
            data = node;
        }
        public InterpreterNode GetData()
        {
            return data;
        }
        public void Add(InterpreterTree item)
        {
            localChildren.Add(item);
        }
        public List<InterpreterTree> GetLocalChildren()
        {
            return localChildren;
        }
    }
}
