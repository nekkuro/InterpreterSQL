﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpreterSQL
{
    static class Constant
    {
        public enum LexemType { SYSTEM, KEYWORD, LITERAL, ASTERIKS, OPERATOR, COMMA, EOF, COLUMN, TABLE, IDENTIFICATOR, TEXT, NUMBER, SELECT, FROM, WHERE, EXPR, AND, OR };
    }
}
